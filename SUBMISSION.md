# Who Run The World

- Jimmy HAGGEGE
- Robin GUERINET

## Fonctionnement du projet

1. Lancer le pipeline CI/CD pour build les images nécessaires au projet, les push sur la registry docker hub puis lancer les manifest nécessaires pour le déploiement de l'application sur le cluster Kubernetes (sur le serveur master)

2. L'application est déployée sur le cluster Kubernetes et est maintenant accessible.
   - On peut le vérifier avec la commande : **kubectl get pods -A**
   - On peut également vérifier les  nodes avec la commande : **kubectl get nodes -A**